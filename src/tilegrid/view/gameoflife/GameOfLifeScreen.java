/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.view.gameoflife;

import tilegrid.model.grid.Grid;
import tilegrid.model.tile.Tile;
import tilegrid.model.tile.togglecell.ToggleCell;
import tilegrid.view.Screen;

/**
 *
 * @author luiz
 */
public class GameOfLifeScreen extends Screen {

    protected final int cellSize;
    protected Grid<ToggleCell> grid;
    protected final int nCols;
    protected final int nRows;
    protected final Tile.Types tileType;

    protected GameOfLifeScreen(int width, int height, int xMargin, int yMargin, int cellSize, Tile.Types type) {
        super(width, height, xMargin, yMargin);
        this.cellSize = cellSize;
        this.tileType = type;
        switch (this.tileType) {
            case RECTANGULAR:
                nCols = (this.width - 2 * this.xMargin) / this.cellSize;
                nRows = (this.height - 2 * this.yMargin) / this.cellSize;
                break;
            case HEXAGONAL:
                nCols = (int) ((this.width - 2 * this.xMargin) / (.866 * this.cellSize));
                double tempNRows = ((this.height - 2*this.yMargin - .25 * this.cellSize) / (.75 * this.cellSize));
                if (this.height - 2*this.yMargin - .25*this.cellSize - (tempNRows+1)*1.5*this.cellSize - .5*this.cellSize*(((int)tempNRows + 1)%2) >= cellSize/2) tempNRows++; //mágica
                nRows = (int) tempNRows;
                break;
            case TRIANGULAR:
                nCols = (int) ((2.0*(this.width - 2*this.xMargin))/this.cellSize);
                nRows = (int) ((this.height - 2*this.yMargin)/(.866*this.cellSize));
                break;
            default:
                throw new RuntimeException("Case not supported.");
        }
    }

    @Override
    public void initialize() {
        this.grid = this.drawGrid(xMargin, yMargin, cellSize, cellSize, nCols, nRows);
        this.update();
    }

    @Override
    public void update() {

        System.out.println("Frame: " + frameCount);
        frameCount++;
        this.getChildren().clear();
        this.getChildren().addAll(this.backgroundElements);
        grid.forEach(cell -> {
            int liveNeighbors = cell.countActiveNeighbors(grid);
            if (cell.isActive()) {
                if (liveNeighbors < 2 || liveNeighbors > 3) {
                    cell.die();
                }
            } else {
                if (liveNeighbors == 3) {
                    cell.ressurect();
                }
            }
        });
        grid.forEach(cell -> cell.update());

    }

    public Grid<ToggleCell> drawGrid(double xMargin, double yMargin, double cellWidth, double cellHeight, int nCols, int nRows) {
        Grid<ToggleCell> cellGrid = new Grid<>(nCols, nRows);
        for (int i = 0; i < nCols; i++) {
            for (int j = 0; j < nRows; j++) {
                ToggleCell cell = newCell(i, j);
                cell.setOnMouseClicked(event -> {
                    cell.toggleActive();
                });
                cell.addTileToList(backgroundElements);
                cellGrid.set(i, j, cell);
            }
        }
        return cellGrid;
    }

    protected ToggleCell newCell(int i, int j) {
        return new ToggleCell(i, j, this.xMargin, this.yMargin, this.cellSize, this.tileType);
    }
    
        public static class GameOfLifeScreenFactory {

        int width = 600;
        int height = 600;
        int xMargin = 30;
        int yMargin = 30;
        int cellSize = 30;
        Tile.Types tileType = Tile.Types.RECTANGULAR;

        public GameOfLifeScreenFactory setWidth(int w) {
            if (w <= 0) return this;
            this.width = w;
            return this;
        }

        public GameOfLifeScreenFactory setHeight(int h) {
            if (h <= 0) return this;
            this.height = h;
            return this;
        }

        public GameOfLifeScreenFactory setxMargin(int x) {
            if (x < 0) return this;
            this.xMargin = x;
            return this;
        }

        public GameOfLifeScreenFactory setyMargin(int y) {
            if (y < 0) return this;
            this.yMargin = y;
            return this;
        }

        public GameOfLifeScreenFactory setCellSize(int s) {
            if (s <= 0) return this;
            this.cellSize = s;
            return this;
        }
        
        public GameOfLifeScreenFactory setTileType(Tile.Types type) {
            this.tileType = type;
            return this;
        }

        public GameOfLifeScreen make() {
            return new GameOfLifeScreen(this.width, this.height, this.xMargin, this.yMargin, this.cellSize, this.tileType);
        }

    }

}
