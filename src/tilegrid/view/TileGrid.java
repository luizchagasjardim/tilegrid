/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.view;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import tilegrid.model.tile.Tile;
import tilegrid.view.gameoflife.GameOfLifeScreen;

/**
 *
 * @author luiz
 */
public class TileGrid extends Application {

    Timer timer = new Timer();
    TimerTask task;

    @Override
    public void start(Stage stage) throws IOException {

        GameOfLifeScreen screen = new GameOfLifeScreen.GameOfLifeScreenFactory()
                .setTileType(Tile.Types.HEXAGONAL)
                .make();

        screen.initialize();

        Scene scene = new Scene(screen, screen.getScreenWidth(), screen.getScreenHeight(), Color.WHITESMOKE);

        stage.setOnCloseRequest(e -> {
            System.exit(0);
        });
        stage.setTitle("Tile Grid");
        stage.setScene(scene);
        stage.show();

        final Timeline timeline = new Timeline(
                new KeyFrame(
                        Duration.millis(500),
                        event -> {
                            screen.update();
                        }
                )
        );
        timeline.setCycleCount(Animation.INDEFINITE);

        scene.setOnKeyPressed((KeyEvent e) -> {
            System.out.println("Key pressed: " + e.getText());
            switch (e.getCode()) {
                case N:
                    screen.update();
                    break;
                case R:
                    screen.initialize();
                    break;
                case P:
                    timeline.play();
                    break;
                case S:
                    timeline.stop();
                    break;
            }
        });

    }

    public static void main(String[] args) {
        launch(args);
    }
}
