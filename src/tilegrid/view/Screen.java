/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.view;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 *
 * @author luiz
 */
public abstract class Screen extends Pane {

    protected final int width;
    protected final int height;
    protected final int xMargin;
    protected final int yMargin;
    protected final List<Node> backgroundElements = new ArrayList<>();
    protected int frameCount = 0;

    protected Screen(int width, int height, int xMargin, int yMargin) {
        this.width = width;
        this.height = height;
        this.xMargin = xMargin;
        this.yMargin = yMargin;
    }

    public abstract void initialize();

    public abstract void update();

    public int getFrameCount() {
        return this.frameCount;
    }

    public int getScreenWidth() {
        return width;
    }

    public int getScreenHeight() {
        return height;
    }

}
