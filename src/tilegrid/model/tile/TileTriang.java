/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.tile;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeType;
import tilegrid.model.grid.Coordinate;

/**
 *
 * @author luiz
 */
public class TileTriang extends Tile<Polygon> {

    public TileTriang(int i, int j, double xMargin, double yMargin, double side) {
        super(i, j);
        this.tile = new Polygon();
        if ((i+j)%2 == 0) {
            //two vertexes up
            double x = xMargin + (i/2)*side + (j%2)*(side/2);
            double y = yMargin + .866*j*side;
            this.tile.getPoints().addAll(
                    x, y,
                    x + side, y,
                    x + side/2, y + .866*side
            );
        } else {
            //one vertex up
            double x = xMargin + ((i+1)/2)*side + (j%2)*(side/2);
            double y = yMargin + .866*j*side;
            this.tile.getPoints().addAll(
                    x, y,
                    x + side/2, y + .866*side,
                    x - side/2, y + .866*side
            );
        }
        this.tile.setStrokeWidth(2);
        this.tile.setStrokeType(StrokeType.CENTERED);
        this.tile.setStroke(Color.BLACK);
        this.tile.setFill(Tile.color);
    }

    @Override
    public List<Coordinate> getNeighborCoordinates() {
        List<Coordinate> neighborCoordinates = new ArrayList<>();
            neighborCoordinates.add(new Coordinate(i+1, j));
            neighborCoordinates.add(new Coordinate(i-1, j));
        if ((i+j) % 2 == 0) {
            neighborCoordinates.add(new Coordinate(i, j-1));
        } else {
            neighborCoordinates.add(new Coordinate(i, j+1));
        }
        return neighborCoordinates;
    }

    @Override
    public <T extends TileInterface> int distanceTo(T t) {
        //TODO
        return Math.max(Math.abs(t.getX()-i), Math.abs(t.getY()-j));
    }

}
