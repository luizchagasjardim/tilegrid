/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.tile;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeType;
import tilegrid.model.grid.Coordinate;

/**
 *
 * @author luiz
 */
public class TileHex extends Tile<Polygon> {

    public TileHex(int i, int j, double xMargin, double yMargin, double side) {
        super(i, j);
        this.tile = new Polygon();
        double xCenter;
        if (j % 2 == 0) {
            xCenter = xMargin + 1.73*(i+0.5)*side;
        } else {
            xCenter = xMargin + 1.73*(i+1)*side;
        }
        double yCenter = yMargin + (1.5*j+1)*side;
        this.tile.getPoints().addAll(
                xCenter, yCenter-side,
                xCenter+.866*side, yCenter-side/2,
                xCenter+.866*side, yCenter+side/2,
                xCenter, yCenter+side,
                xCenter-.866*side, yCenter+side/2,
                xCenter-.866*side, yCenter-side/2
        );
        this.tile.setStrokeWidth(2);
        this.tile.setStrokeType(StrokeType.CENTERED);
        this.tile.setStroke(Color.BLACK);
        this.tile.setFill(Tile.color);
    }

    @Override
    public List<Coordinate> getNeighborCoordinates() {
        List<Coordinate> neighborCoordinates = new ArrayList<>();
            neighborCoordinates.add(new Coordinate(i+1, j));
            neighborCoordinates.add(new Coordinate(i-1, j));
            neighborCoordinates.add(new Coordinate(i, j+1));
            neighborCoordinates.add(new Coordinate(i, j-1));
        if (j % 2 == 0) {
            neighborCoordinates.add(new Coordinate(i-1, j+1));
            neighborCoordinates.add(new Coordinate(i-1, j-1));
        } else {
            neighborCoordinates.add(new Coordinate(i+1, j+1));
            neighborCoordinates.add(new Coordinate(i+1, j-1));
        }
        return neighborCoordinates;
    }

    @Override
    public <T extends TileInterface> int distanceTo(T t) {
        //TODO
        return Math.max(Math.abs(t.getX()-i), Math.abs(t.getY()-j));
    }

}
