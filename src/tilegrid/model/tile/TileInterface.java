/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.tile;

import java.util.List;
import tilegrid.model.grid.Coordinate;
import tilegrid.model.grid.Grid;

/**
 *
 * @author luiz
 */
public interface TileInterface {
    public Coordinate getCoordinates();
    public int getX();
    public int getY();
    public <T extends TileInterface> int distanceTo(T t);
    public List<Coordinate> getNeighborCoordinates();
    public <T extends TileInterface> List<T> getNeighbors(Grid<T> g);
    public <T extends TileInterface> boolean isNeighbor(T n);
//    public <T extends TileInterface> List<T> getRange(Grid<T> g, int range);
    public <T extends TileInterface> boolean isInRange(T n, int range);
}
