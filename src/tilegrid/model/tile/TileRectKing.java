/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.tile;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import tilegrid.model.grid.Coordinate;

/**
 *
 * @author luiz
 */
public class TileRectKing extends Tile<Rectangle> {

    public TileRectKing(int i, int j, double xMargin, double yMargin, double width, double height) {
        super(i, j);
        this.tile = new Rectangle(xMargin + i * width, yMargin + j * height, width, height);
        this.tile.setStrokeWidth(2);
        this.tile.setStrokeType(StrokeType.CENTERED);
        this.tile.setStroke(Color.BLACK);
        this.tile.setFill(Tile.color);
    }

    @Override
    public List<Coordinate> getNeighborCoordinates() {
        List<Coordinate> neighborCoordinates = new ArrayList<>();
        neighborCoordinates.add(new Coordinate(i+1, j));
        neighborCoordinates.add(new Coordinate(i+1, j+1));
        neighborCoordinates.add(new Coordinate(i, j+1));
        neighborCoordinates.add(new Coordinate(i-1, j+1));
        neighborCoordinates.add(new Coordinate(i-1, j));
        neighborCoordinates.add(new Coordinate(i-1, j-1));
        neighborCoordinates.add(new Coordinate(i, j-1));
        neighborCoordinates.add(new Coordinate(i+1, j-1));
        return neighborCoordinates;
    }

    @Override
    public <T extends TileInterface> int distanceTo(T t) {
        return Math.max(Math.abs(t.getX()-i), Math.abs(t.getY()-j));
    }

}
