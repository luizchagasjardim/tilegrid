/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.tile;

import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import tilegrid.model.grid.Coordinate;
import tilegrid.model.grid.Grid;

/**
 *
 * @author luiz
 * @param <T>
 */
public abstract class Tile<T extends Shape> implements TileInterface {

    public static enum Types {
        RECTANGULAR,
        HEXAGONAL,
        TRIANGULAR
    }

    protected static final Color color = Color.PALETURQUOISE;

    protected final int i;
    protected final int j;
    protected T tile;

    public Tile(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public void setColor(Color color) {
        this.tile.setFill(color);
    }

    public void resetColor() {
        this.tile.setFill(color);
    }

    public void setOnMouseClicked(EventHandler<? super MouseEvent> h) {
        tile.setOnMouseClicked(h);
    }

    public void addTileToList(List<Node> list) {
        list.add(this.tile);
    }

    @Override
    public Coordinate getCoordinates() {
        return new Coordinate(this.i, this.j);
    }

    @Override
    public int getX() {
        return this.i;
    }

    @Override
    public int getY() {
        return this.j;
    }

    @Override
    public <U extends TileInterface> List<U> getNeighbors(Grid<U> g) {
        return g.getNeighbors(i, j);
    }

    @Override
    public <U extends TileInterface> boolean isNeighbor(U n) {
        return this.distanceTo(n) == 1;
    }

//    @Override
//    public <U extends TileInterface> List<U> getRange(Grid<U> g) {
//        return g.getRange(i,j);
//    }
    @Override
    public <T extends TileInterface> boolean isInRange(T n, int range) {
        return (this.distanceTo(n) < range);
    }
}
