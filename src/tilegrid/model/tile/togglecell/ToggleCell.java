/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.tile.togglecell;

import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import tilegrid.model.grid.Coordinate;
import tilegrid.model.grid.Grid;
import tilegrid.model.tile.Tile;
import tilegrid.model.tile.TileHex;
import tilegrid.model.tile.TileInterface;
import tilegrid.model.tile.TileRectKing;
import tilegrid.model.tile.TileTriang;

/**
 *
 * @author luiz
 */
public class ToggleCell implements TileInterface {

    protected Tile tile;
    protected static final Color deadColor = Color.BLUEVIOLET;
    protected final BooleanProperty active = new SimpleBooleanProperty(Boolean.FALSE);
    protected boolean nextState = false;
    
    public ToggleCell(int i, int j, double xMargin, double yMargin, double size, Tile.Types type) {
        switch(type) {
            case RECTANGULAR:
                tile = new TileRectKing(i, j, xMargin, yMargin, size, size);
                break;
            case HEXAGONAL:
                tile = new TileHex(i, j, xMargin, yMargin, size/2);
                break;
            case TRIANGULAR:
                tile = new TileTriang(i, j, xMargin, yMargin, size);
                break;
            default:
                throw new RuntimeException("Case not supported.");
                
        }
        this.active.addListener(event -> {
            if (this.active.getValue()) {
                this.tile.setColor(ToggleCell.deadColor);
                System.out.println("Clicked: (" + i + ", " + j + ")");
            } else {
                this.tile.resetColor();
            }
        });
    }

    public void ressurect() {
        this.nextState = true;
    }

    public void die() {
        this.nextState = false;
    }

    public void toggleActive() {
        this.active.setValue(this.active.not().getValue());
        this.nextState = this.active.getValue();
    }

    public void update() {
        this.active.setValue(this.nextState);
    }

    public boolean isActive() {
        return this.active.getValue();
    }

    public void setOnMouseClicked(EventHandler<? super MouseEvent> h) {
        tile.setOnMouseClicked(h);
    }

    public void addTileToList(List<Node> list) {
        tile.addTileToList(list);
    }

    @Override
    public List<Coordinate> getNeighborCoordinates() {
        return tile.getNeighborCoordinates();
    }
    
    @Override
    public List<ToggleCell> getNeighbors(Grid g) {
        return g.getNeighbors(tile.getX(), tile.getY());
    }
    
    public int countActiveNeighbors(Grid g) {
        int activeNeighbors = 0;
        List<ToggleCell> neighbors = g.getNeighbors(tile.getX(), tile.getY());
        activeNeighbors = neighbors.stream().filter((n) -> (n.isActive())).map((_item) -> 1).reduce(activeNeighbors, Integer::sum);
        return activeNeighbors;
    }

    @Override
    public Coordinate getCoordinates() {
        return tile.getCoordinates();
    }

    @Override
    public int getX() {
        return tile.getX();
    }

    @Override
    public int getY() {
        return tile.getY();
    }

    @Override
    public <T extends TileInterface> int distanceTo(T t) {
        return tile.distanceTo(t);
    }

    @Override
    public <T extends TileInterface> boolean isNeighbor(T n) {
        return tile.isNeighbor(n);
    }

    @Override
    public <T extends TileInterface> boolean isInRange(T n, int range) {
        return tile.isInRange(n, range);
    }
}
