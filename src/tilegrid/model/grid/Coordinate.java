/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.grid;

import java.util.Objects;

/**
 *
 * @author luiz
 */
public class Coordinate {

        int i;
        int j;

        public Coordinate(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.i, this.j);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Coordinate other = (Coordinate) obj;
            if (this.i != other.i) {
                return false;
            }
            if (this.j != other.j) {
                return false;
            }
            return true;
        }

    
}
