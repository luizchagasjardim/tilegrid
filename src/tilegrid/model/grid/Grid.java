/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilegrid.model.grid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import tilegrid.model.tile.TileInterface;

/**
 *
 * @author luiz
 * @param <T>
 */
public class Grid<T extends TileInterface> {

    //this implementation might be changed later
    private final Map<Coordinate, T> tileTable; //TODO: elements can be accessed from outside of this class
    private final int nCols;
    private final int nRows;

    public Grid(int nCols, int nRows) {
        this.nCols = nCols;
        this.nRows = nRows;
        tileTable = new HashMap<>();
    }

    public T get(int i, int j) {
        return this.get(new Coordinate(i, j));
    }
    
    public T get(Coordinate coord) {
        T cell = tileTable.get(coord);
        if (cell == null) throw new RuntimeException("No tile at coordinates (" + coord.i + ", " + coord.j + ").");
        return tileTable.get(coord);
    }

    public void set(int i, int j, T tile) {
        if (i < 0 || i > nCols || j < 0 || j > nRows) {
            throw new RuntimeException("Position (" + i + "," + j + ") out of bounds.");
        }
        tileTable.put(new Coordinate(i, j), tile);
    }

    public List<T> getNeighbors(int i, int j) {
        List<T> neighbors = new ArrayList<>();
        List<Coordinate> neighborCoordinates = tileTable.get(new Coordinate(i, j)).getNeighborCoordinates();
        for (Coordinate coord : neighborCoordinates) {
            try {
                neighbors.add(this.get(coord));
            } catch (RuntimeException e) {
                //ignore if tile does not exist
            }
        }
        return neighbors;
    }
    
    public void forEach(Consumer<T> action) {
        for (Map.Entry<Coordinate, T> entry : tileTable.entrySet()) {
            action.accept(entry.getValue());
        }
    }
    
}